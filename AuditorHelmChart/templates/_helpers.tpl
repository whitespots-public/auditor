{{- define "appsecauditor.postgres.password" -}}
{{- if .Values.postgresql.enabled }}
{{- if (eq "" .Values.postgresql.auth.existingSecret )}}
DB_PASS: {{ .Values.postgresql.auth.password | b64enc | quote }}
{{- end }}
{{- else if .Values.externalPostgresql.enabled }}
{{- if (eq "" .Values.externalPostgresql.existingSecret )}}
DB_PASS: {{ .Values.externalPostgresql.password | b64enc | quote }}
{{- end }}
{{- end }}
{{- end -}}

{{- define "appsecauditor.secret.amqp" -}}

{{- if .Values.rabbitmq.enabled }}
{{- if not (empty .Values.rabbitmq.auth.password) -}}
AMQP_HOST_STRING: {{ printf "amqp://%s:%s@%s:%d/%s" .Values.rabbitmq.auth.username .Values.rabbitmq.auth.password (include "rabbitmq.host" . ) (int .Values.rabbitmq.containerPorts.amqp) .Values.rabbitmq.auth.vhost | b64enc | quote }}
{{- else -}}
AMQP_HOST_STRING: {{ printf "amqp://%s:%s@%s:%d/%s" .Values.rabbitmq.auth.username (include "rabbitmq.exist.password") (include "rabbitmq.host" . ) (int .Values.rabbitmq.containerPorts.amqp) .Values.rabbitmq.auth.vhost | b64enc | quote }}
{{- end -}}
{{- else if .Values.externalRabbitmq.enabled }}
{{- if not (empty .Values.externalRabbitmq.password) -}}
AMQP_HOST_STRING: {{ printf "%s://%s:%s@%s:%d/%s" .Values.externalRabbitmq.scheme .Values.externalRabbitmq.username .Values.externalRabbitmq.password .Values.externalRabbitmq.host (int .Values.externalRabbitmq.port) .Values.externalRabbitmq.vhost | b64enc | quote }}
{{- else -}}
AMQP_HOST_STRING: {{ printf "%s://%s:%s@%s:%d/%s" .Values.externalRabbitmq.scheme .Values.externalRabbitmq.username (include "rabbitmq.external.exist.password" . ) .Values.externalRabbitmq.host (int .Values.externalRabbitmq.port) .Values.externalRabbitmq.vhost | b64enc | quote }}
{{- end }}
{{- end }}
{{- end -}}

{{- define "getValueFromSecret" }}
{{- $len := (default 16 .Length) | int -}}
{{- $obj := (lookup "v1" "Secret" .Namespace .Name).data -}}
{{- index $obj .Key | trimAll "\"" | b64dec -}}
{{- end }}

{{- define "rabbitmq.exist.password" -}}
    {{- include "getValueFromSecret" (dict "Namespace" .Release.Namespace "Name" .Values.rabbitmq.auth.existingPasswordSecret "Length" 16 "Key" .Values.rabbitmq.auth.existingSecretPasswordKey)  -}}
{{- end -}}

{{- define "rabbitmq.external.exist.password" -}}
    {{- include "getValueFromSecret" (dict "Namespace" .Release.Namespace "Name" .Values.externalRabbitmq.existingPasswordSecret "Length" 16 "Key" .Values.externalRabbitmq.existingSecretPasswordKey)  -}}
{{- end }}


{{- define "rabbitmq.host"}}
{{- printf "%s-%s" .Release.Name .Values.rabbitmq.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end }}    

{{- define "postgres.host"}}
{{- if .Values.postgresql.enabled }}
{{- printf "%s-%s" .Release.Name .Values.postgresql.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- else if .Values.externalPostgresql.enabled }}
{{- .Values.externalPostgresql.host | quote }}
{{- end }}
{{- end }}

{{- define "postgres.port" }}
{{- if .Values.postgresql.enabled }}
{{- .Values.postgresql.containerPorts.postgresql | quote }}
{{- else if .Values.externalPostgresql.enabled }}
{{- .Values.externalPostgresql.port | quote }}
{{- end }}
{{- end }}

{{- define "postgres.database.name" }}
{{- if .Values.postgresql.enabled }}
{{- .Values.postgresql.auth.database| quote }}
{{- else if .Values.externalPostgresql.enabled }}
{{- .Values.externalPostgresql.database | quote }}
{{- end }}
{{- end }}

{{- define "postgres.user" }}
{{- if .Values.postgresql.enabled }}
{{- .Values.postgresql.auth.username | quote }}
{{- else if .Values.externalPostgresql.enabled }}
{{- .Values.externalPostgresql.username | quote }}
{{- end }}
{{- end }}

{{- define "redis.host" }}
{{- if .Values.redis.enabled }}
{{- printf "%s-%s-%s" .Release.Name .Values.redis.nameOverride "master" | trunc 63 | trimSuffix "-" -}}
{{- else if .Values.externalRedis.enabled }}
{{- .Values.externalRedis.host | quote }}
{{- end }}
{{- end }}

{{- define "appsecauditor.redis.password" -}}
{{- if $.Values.redis.enabled }}
{{- if (eq "" .Values.redis.auth.existingSecret )}}
REDIS_PASSWORD: {{ .Values.redis.auth.password | b64enc | quote }}
{{- end }}
{{- else if .Values.externalRedis.enabled }}
{{- if (eq "" .Values.externalRedis.existingSecret )}}
REDIS_PASSWORD: {{ .Values.externalRedis.password | b64enc | quote }}
{{- end }}
{{- end }}
{{- end -}}


{{/*
Create auditor name
*/}}
{{- define "auditor.fullname" -}}
{{- printf "%s-%s" (include "appsecauditor.fullname" .) .Values.auditor.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create scanner-worker name
*/}}
{{- define "scannerWorker.fullname" -}}
{{- printf "%s-%s" (include "appsecauditor.fullname" .) .Values.scannerWorker.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Create redis name
*/}}
{{- define "redis.fullname" -}}
{{- printf "%s-%s" (include "appsecauditor.fullname" .) .Values.redis.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
